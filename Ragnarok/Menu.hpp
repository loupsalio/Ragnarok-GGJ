#pragma once

#include "Agame_state.hpp"
#include "entity.hpp"

class menu : public Agame_state
{
public:
	explicit menu(sf::RenderWindow &windows, game *my_game);
	virtual ~menu() = default;
	void init() override;
	void traitement() override;
	void events() override;
	void draw() override;
	void pause() override;
};