#pragma once

#include "Agame_state.hpp"
#include "entity.hpp"
#include "entity_type.hpp"
#include "player.hpp"
#include "map.hpp"

class level : public Agame_state
{
	int				number_level_;
	Map				my_map;
	player			my_player;
	sf::View		my_view;
	bool			move_left = false;
	bool			move_right = false;
	bool			move_up = false;
	bool			move_down = false;
	sf::Clock		my_clock_;
public:
	level(int const level_value, sf::RenderWindow &window, game *my_game);
	virtual ~level() = default;
	void	init() override;
	void	traitement() override;
	void	events() override;
	void	draw_map();
	void	draw_player();
	void	draw() override;
	void	collision();
	bool	touch(entity_type const type);
};
