#include <SFML/Graphics.hpp>
#include "Menu.hpp"
#include "game.hpp"
#include "level.hpp"

menu::menu(sf::RenderWindow& windows, game *my_game) : Agame_state(windows, my_game)
{
	// Textures

	add_texture_from_file("cursor", "../textures/cursor.png");
	add_texture_from_file("cursor-on", "../textures/cursor-on.png");
	add_texture_from_file("background", "../textures/background.png");
	add_texture_from_file("game_logo", "../textures/game_name.png");
	add_texture_from_file("button", "../textures/button.png");
	add_texture_from_file("button-press", "../textures/button-press.png");

	texture_["background"]->setRepeated(true);

	// Entit�es

	add_entity("background", "background");
	add_entity("game_logo", "game_logo");
	add_entity("button_play", "button");
	add_entity("cursor", "cursor-on");

	entity_["button_play"]->add_texture(texture_["button-press"]);
	entity_["cursor"]->add_texture(texture_["cursor"]);

	// Taille du sprite avec la texture

	entity_["game_logo"]->get_sprite().setScale(sf::Vector2f(1.2f, 1.2f));
	entity_["cursor"]->get_sprite().setScale(sf::Vector2f(0.5f, 0.5f));

	// D�coupage des sprites sur entit�es

	entity_["background"]->get_sprite().setTextureRect({ 0, 0, 2560, 1280 });

	// Position des entit�es

	entity_["button_play"]->set_position(window_.getSize().x / 2 - (texture_["button"]->getSize().x /2) - 20, window_.getSize().y / 2 - (texture_["button"]->getSize().y / 2));
	entity_["game_logo"]->set_position(160, 50);

	// Definition de la Musique

	if (!music_.openFromFile("../musics/menu.ogg")) {
		std::cerr << "file not found (music menu)" << std::endl;
		throw;
	}
	music_.setVolume(10.f);
	music_.setLoop(true);
}

void menu::init()
{
	window_.setView(window_.getDefaultView());
	my_game_->delete_state("game");
	my_game_->set_state("game", new level(1, window_, my_game_));
	music_.play();
}


void menu::traitement()
{
	// Mouvement du background
	entity_["background"]->set_position(entity_["background"]->get_position().x - 1, 0);
	if (entity_["background"]->get_position().x <= -1280)
		entity_["background"]->set_position(0.f, 0.f);
	
	// curseur
	sf::Vector2i pos_mouse = sf::Mouse::getPosition(window_);
	entity_["cursor"]->set_position(pos_mouse.x, pos_mouse.y);

}

void menu::events()
{
	sf::Event event;
	while (window_.pollEvent(event))
	{
		switch (event.type)
		{

		case sf::Event::Closed:
			window_.close();
			break;


		case sf::Event::TextEntered:
			if (event.text.unicode < 128)
			{
				if (event.text.unicode == '!')
				{
					std::cout << "=====ENTITY LIST=====" << std::endl;
					for (auto & elem : entity_)
						elem.second->info();
				}
			
			}
			break;

		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Right)
			{
				std::cout << "The right button was pressed" << std::endl;
				std::cout << "mouse x: " << event.mouseButton.x << std::endl;
				std::cout << "mouse y: " << event.mouseButton.y << std::endl;

			}
			else if (event.mouseButton.button == sf::Mouse::Left)
			{
				std::cout << "The left button was pressed" << std::endl;
				std::cout << "mouse x: " << event.mouseButton.x << std::endl;
				std::cout << "mouse y: " << event.mouseButton.y << std::endl;
				if (entity_["button_play"]->is_inside(event.mouseButton.x, event.mouseButton.y)) {
					my_game_->change_state("game");
				}
			}
			break;
			
		
					case sf::Event::KeyPressed:
			if (event.key.code == sf::Keyboard::Return)
					my_game_->change_state("game");
			break;
			
		case sf::Event::MouseMoved:
			if (entity_["button_play"]->is_inside(event.mouseMove.x, event.mouseMove.y)) {
				entity_["button_play"]->set_texture(1);
				entity_["cursor"]->set_texture(1);
			}
			else {
				entity_["button_play"]->set_texture(0);
				entity_["cursor"]->set_texture(0);
			}
			break;
		default:
			break;
		}
	}
}

void menu::pause()
{
	music_.pause();
}


void menu::draw()
{
	window_.clear(sf::Color::White);

	// Liste des entit�es � afficher dans l'ordre

	window_.draw(entity_["background"]->get());
	window_.draw(entity_["game_logo"]->get());
	window_.draw(entity_["button_play"]->get());
	window_.draw(entity_["cursor"]->get());

	window_.display();
}


