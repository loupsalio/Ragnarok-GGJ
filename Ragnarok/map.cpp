#include "map.hpp"

Map::Map() : _map(Y, std::vector<int>(X))
{
}

Map::~Map()
{
}

void		Map::setLevel(int value)
{
	_level = value;
	setMap();
}

void		Map::setMap()
{
	switch (_level)
	{
		case (1):
			chargeMap("../map/map1.txt");
			break;
		case (2):
			chargeMap("../map/map2.txt");
			break;
		case (3):
			chargeMap("../map/map3.txt");
			break;
		case (4):
			chargeMap("../map/map4.txt");
			break;
		case (5):
			chargeMap("../map/map5.txt");
			break;
		case (6):
			chargeMap("../map/map6.txt");
			break;
	}
}

void		Map::chargeMap(std::string _filename)
{
	std::ifstream		ifs(_filename);
	std::string			str;
	int					i = 0;
	int					j = 0;
	int					c = 0;

	if (!ifs)
		exit(84);
	std::getline(ifs, str, '\0');
	while (i < Y)
	{
		j = 0;
		while (j < X)
		{
			_map[i][j] = str[c] - 48;
			j++;
			c++;
		}
		c++;
		i++;
	}
}

int			Map::getCase(int x, int y)
{
	return (_map[y][x]);
}