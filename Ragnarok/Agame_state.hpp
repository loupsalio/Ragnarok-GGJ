#pragma once
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include "entity.hpp"

class game;

class Agame_state
{
public:
	Agame_state(sf::RenderWindow &window, game *my_game);
	virtual ~Agame_state() = default;
	virtual void	init() = 0;
	virtual void	traitement() = 0;
	virtual void	events() = 0;
	virtual void	draw() = 0;
	virtual void	pause();
	void			add_texture_from_file(std::string const &name, std::string const &file);
	void			add_entity(std::string const &name, std::string const &texture_name);
	void			add_entity(std::string const &name, entity &);
	protected:
	sf::RenderWindow								&window_;
	std::map<std::string, sf::Texture*>				texture_;
	std::map<std::string, entity*>					entity_;
	game											*my_game_;
	sf::Music										music_;
};
