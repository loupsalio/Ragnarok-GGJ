#include "entity.hpp"

entity::entity(std::string name) : name_(name) {};

entity::entity(entity const &elem, int const texture_id){
	name_ = elem.get_name();
	texture_id_ = texture_id;
	sprite_ = elem.copy_sprite();
	texture_ = elem.copy_textures();
	std::cout << "Entity \"" << name_ << "\" created" << std::endl;
}

void	entity::add_texture(sf::Texture *elem) {
	texture_.push_back(elem);
}

sf::Sprite &entity::get_sprite() {
	return sprite_;
}

sf::Sprite entity::copy_sprite() const {
	return sprite_;
}

std::vector<sf::Texture*>	entity::copy_textures() const {
	return texture_;
}

std::string entity::get_name() const {
	return name_;
}

void	entity::set_name(std::string const &name) {
	name_ = name;
}

void	entity::next_texture() {
	texture_id_++;
	if (texture_id_ >= texture_.size())
		texture_id_ = 0;
}

void	entity::previous_texture() {
	texture_id_--;
	if (texture_id_ < 0)
		texture_id_ = texture_.size();
}

void	entity::set_position(float x, float y) {
	x_ = x;
	y_ = y;
}

sf::Vector2f	const			entity::get_position() const{
	return sf::Vector2f(x_, y_);
}

sf::Sprite const				&entity::get() {
	sprite_.setTexture(*texture_.at(texture_id_));
	sprite_.setPosition(x_, y_);
	return sprite_;
}

void	entity::set_texture(int i) {
	texture_id_ = i;
}

void	entity::info() const {
	std::cout << "[" << name_ << "](" << texture_id_ << ") : " << x_ << " | " << y_ << std::endl;
}

bool	entity::is_inside(float const x, float const y) {
	return sprite_.getGlobalBounds().contains(x, y);
}