#include "player.hpp"

player::player(std::string const &name_entity) : entity(name_entity)
{
	position_.x = 1.f;
	position_.y = 1.f;
	std::cout << "player created" << std::endl;
}

void player::move_up()
{
	position_.y -= MOVE_SPEED;
}

void player::move_down()
{
	position_.y += MOVE_SPEED;
}

void player::move_right()
{
	position_.x += MOVE_SPEED;
}

void player::move_left()
{
	position_.x -= MOVE_SPEED;
}

sf::Vector2f const &player::getPosition() const
{
	return position_;
}

void	player::lost_life()
{
	life_--;
}

int player::get_life() const
{
	return life_;
}

void player::set_player_position(int const x, int const y)
{
	position_.x = x;
	position_.y = y;
}
