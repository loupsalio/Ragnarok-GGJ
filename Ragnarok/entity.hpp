#pragma once

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

# define ENTITY_SIZE	16

class entity
{
public:
	entity() = default;
	entity(std::string name);
	entity(entity const &elem, int const texture_id = 0);
	virtual ~entity() = default;
	void								add_texture(sf::Texture*);
	std::string							get_name() const;
	void								set_name(std::string const &name);
	virtual void						next_texture();
	virtual void						previous_texture();
	virtual void						set_texture(int i);
	sf::Sprite							&get_sprite();
	sf::Sprite							copy_sprite() const;
	std::vector<sf::Texture*>			copy_textures() const;
	sf::Sprite	const					&get();
	void								set_position(float x, float y);
	sf::Vector2f	const				get_position() const;
	void								info() const;
	bool								is_inside(float const x, float const y);

protected:
	std::string					name_;
	unsigned int				texture_id_ = 0;
	float						x_ = 0;
	float						y_ = 0;
	std::vector<sf::Texture*>	texture_;
	sf::Sprite					sprite_;
	sf::SoundBuffer				buffer_;
	sf::Sound					sound_;
};
