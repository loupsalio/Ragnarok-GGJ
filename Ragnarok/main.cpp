#include "game.hpp"

int main()
{
	game	my_game(1280, 640, "ragnarok", sf::Style::Close | sf::Style::Titlebar);
	my_game.set_vsync(true);
	my_game.set_cursor(false);
	my_game.run();

	return 0;
}

