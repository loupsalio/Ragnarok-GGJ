#include "level.hpp"
#include "game.hpp"
#include "Menu.hpp"

game::game(int const width, int const  heigh, std::string const &name, int const vm)
: window_(sf::VideoMode(width, heigh), name, vm)
{
	std::cout << name << " Ready" << std::endl;
	init_game();
}

game::~game()
{
	for (auto & state : states_)
	{
		delete state.second;
	}
}

bool game::get_vsync() const
{
	return vsync_;
}

void game::set_vsync(bool const status)
{
	vsync_ = status;
	window_.setVerticalSyncEnabled(status);
}

int game::get_fps_limit() const
{
	return fps_limit_;
}

void game::set_fps_limit(int const value)
{
	fps_limit_ = value;
	set_vsync(false);
	window_.setFramerateLimit(value);
}

bool game::is_running() const
{
	return running_;
}

void game::set_state(std::string name, Agame_state *elem)
{
	states_[name] = elem;
}

void	game::change_state(std::string const& name)
{
	for (auto & state : states_)
	{
		if (state.first == name) {
			current_state_->pause();
			current_state_ = state.second;
			current_state_->init();
			break;
		}
	}
}

Agame_state* game::get_state(std::string const& name) const
{
	for (auto & state : states_)
	{
		if (state.first == name)
			return state.second;
	}
	return nullptr;
}

void game::delete_state(std::string const& name)
{
	for (auto state = states_.begin(); state != states_.end(); state++)
	{
		if ((*state).first == name) {
			delete (*state).second;
			states_.erase(state);
			break;
		}
	}
}


void game::set_cursor(bool const value)
{
	window_.setMouseCursorVisible(value);
}

void game::init_game()
{
	set_cursor(false);

	// Game_states
	set_state("menu", new menu(window_, this));
}

void game::run()
{
	running_ = true;
	try {
		current_state_ = get_state("menu");
	}
	catch(std::exception e)
	{
		std::cerr << e.what() << std::endl;
	}
	if (current_state_)
		current_state_->init();
	while (window_.isOpen() && current_state_ != nullptr)
	{
		current_state_->traitement();
		current_state_->events();
		current_state_->draw();
	}
	running_ = false;
}
