#include "Agame_state.hpp"

Agame_state::Agame_state(sf::RenderWindow& window, game *my_game)
	: window_(window), my_game_(my_game)
{
}

void Agame_state::add_texture_from_file(std::string const &name, std::string const &file)
{
	texture_[name] = new sf::Texture;
	if (!texture_[name]->loadFromFile(file)) {
		std::cerr << "Can't load " << file << " file !" << std::endl;
		delete texture_[name];
		texture_[name] = nullptr;
		throw;
	}
	texture_[name]->setSmooth(true);
}

void Agame_state::add_entity(std::string const &name, std::string const &texture_name)
{
	entity_[name] = new entity(name);
	entity_[name]->add_texture(texture_[texture_name]);
}

void Agame_state::add_entity(std::string const &name, entity &new_entity)
{
	entity_[name] = &new_entity;
}

void Agame_state::pause()
{
	
}
