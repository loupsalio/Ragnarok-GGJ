#include "level.hpp"
#include "game.hpp"

level::level(int const level_value, sf::RenderWindow &window, game *my_game) 
: Agame_state(window, my_game), my_player("player_entity")
{
	my_map.setLevel(level_value);
}


void level::init()
{
	add_texture_from_file("free", "../textures/free.png");
	add_texture_from_file("block", "../textures/block.png");
	add_texture_from_file("portoloin", "../textures/portoloin.png");
	add_texture_from_file("player1", "../textures/player.png");
	add_texture_from_file("player2", "../textures/player2.png");
	add_texture_from_file("player3", "../textures/player3.png");
	add_texture_from_file("player4", "../textures/player4.png");
	add_texture_from_file("player5", "../textures/player5.png");
	add_texture_from_file("player6", "../textures/player6.png");
	add_texture_from_file("finish1", "../textures/finish.png");
	add_texture_from_file("finish2", "../textures/finish2.png");
	add_texture_from_file("finish3", "../textures/finish3.png");
	add_texture_from_file("finish4", "../textures/finish4.png");
	add_texture_from_file("finish5", "../textures/finish5.png");
	add_texture_from_file("finish6", "../textures/finish6.png");

	add_entity("free", "free");
	add_entity("block", "block");
	add_entity("portoloin", "portoloin");
	add_entity("finish", "finish1");
	add_entity("player", my_player);


	entity_["player"]->add_texture(texture_["player1"]);
	entity_["player"]->add_texture(texture_["player2"]);
	entity_["player"]->add_texture(texture_["player3"]);
	entity_["player"]->add_texture(texture_["player4"]);
	entity_["player"]->add_texture(texture_["player5"]);
	entity_["player"]->add_texture(texture_["player6"]);

	entity_["finish"]->add_texture(texture_["finish1"]);
	entity_["finish"]->add_texture(texture_["finish2"]);
	entity_["finish"]->add_texture(texture_["finish3"]);
	entity_["finish"]->add_texture(texture_["finish4"]);
	entity_["finish"]->add_texture(texture_["finish5"]);
	entity_["finish"]->add_texture(texture_["finish6"]);
	my_view.setCenter(sf::Vector2f((my_player.getPosition().x * ENTITY_SIZE) + 8, (my_player.getPosition().y * ENTITY_SIZE) + 8));
	my_view.setSize(sf::Vector2f(320, 160));
	window_.setView(my_view);
}

void level::traitement()
{
	my_view.setCenter((my_player.getPosition().x * ENTITY_SIZE) + 8, (my_player.getPosition().y * ENTITY_SIZE) + 8);
	window_.setView(my_view);
	sf::Time time_elapsed = my_clock_.getElapsedTime();
	if (time_elapsed.asMilliseconds() > 400) {
		entity_["player"]->next_texture();
		entity_["finish"]->next_texture();
		my_clock_.restart();
	}
}

void level::events()
{
	sf::Event event;
	while (window_.pollEvent(event))
	{
		switch (event.type)
		{

		case sf::Event::Closed:
			window_.close();
			break;

		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Right)
			{
				std::cout << "The right button was pressed" << std::endl;
				std::cout << "mouse x: " << event.mouseButton.x << std::endl;
				std::cout << "mouse y: " << event.mouseButton.y << std::endl;

			}
			else if (event.mouseButton.button == sf::Mouse::Left)
			{
				std::cout << "The left button was pressed" << std::endl;
				std::cout << "mouse x: " << event.mouseButton.x << std::endl;
				std::cout << "mouse y: " << event.mouseButton.y << std::endl;
			}
			break;

		case sf::Event::TextEntered:
			if (event.text.unicode < 128)
			{
				if (event.text.unicode == '!')
				{
					std::cout << "=====ENTITY LIST=====" << std::endl;
					for (auto & elem : entity_)
						elem.second->info();
				}


				else if (event.text.unicode == '*')
				{
					std::cout << "=====PLAYER=====" << std::endl;
					std::cout << my_player.getPosition().x << " | " << my_player.getPosition().y << std::endl;
					std::cout << "left : " << my_player.getPosition().x - (MOVE_SPEED) << " | " << my_player.getPosition().y << std::endl;
					std::cout << "right : " << my_player.getPosition().x + (MOVE_SPEED) + 1 << " | " << my_player.getPosition().y << std::endl;
					std::cout << "up : " << my_player.getPosition().x << " | " << my_player.getPosition().y - (MOVE_SPEED) << std::endl;
					std::cout << "down : " << my_player.getPosition().x << " | " << my_player.getPosition().y + (MOVE_SPEED) + 1 << std::endl;
				}
			}
			break;

		case sf::Event::KeyPressed:
			switch (event.key.code) {
			case sf::Keyboard::Escape:
				my_game_->change_state("menu");
				return;
				break;
			case sf::Keyboard::D:
				move_right = true;
				break;
			case sf::Keyboard::Q:
				move_left = true;
				break;
			case sf::Keyboard::Z:
				move_up = true;
				break;
			case sf::Keyboard::S:
				move_down = true;
				break;
			}
			break;
		case sf::Event::KeyReleased:
			switch (event.key.code) {
			case sf::Keyboard::D:
				move_right = false;
				break;
			case sf::Keyboard::Q:
				move_left = false;
				break;
			case sf::Keyboard::Z:
				move_up = false;
				break;
			case sf::Keyboard::S:
				move_down = false;
				break;
			}
			break;

		case sf::Event::MouseMoved:
	
			break;
		default:
			break;
		}
	}

	if (touch(Portoloin)) {
		std::cout << "tp" << std::endl;
		std::srand(std::time(nullptr)); 
		int rand_x = std::rand() % 39;
		int rand_y = std::rand() % 39;
		while(my_map.getCase(rand_x, rand_y) != Free)
		{
			rand_x = std::rand() % 39;
			rand_y = std::rand() % 39;
		}
		my_player.set_player_position(rand_x, rand_y);
	}
	if (touch(Finish)) {
		my_game_->change_state("menu");
		std::cout << "win" << std::endl;
	}
	collision();
}

bool level::touch(entity_type const type)
{
/*	if (my_player.getPosition().x >= 40 || my_player.getPosition().x < 0 || my_player.getPosition().y >= 40 || my_player.getPosition().y < 0)
		return false;*/
	sf::Vector2f	point_top_left;
	sf::Vector2f	point_top_right;
	sf::Vector2f	point_down_left;
	sf::Vector2f	point_down_right;

	point_top_left.x = my_player.getPosition().x + 0.1;
	point_top_left.y = my_player.getPosition().y + 0.1;
	point_top_right.x = my_player.getPosition().x + 0.9;
	point_top_right.y = my_player.getPosition().y + 0.1;
	point_down_left.x = my_player.getPosition().x + 0.1;
	point_down_left.y = my_player.getPosition().y + 0.9;
	point_down_right.x = my_player.getPosition().x + 0.9;
	point_down_right.y = my_player.getPosition().y + 0.9;

	if (move_left && my_map.getCase(static_cast<int>(point_top_left.x - (MOVE_SPEED)), static_cast<int>(point_top_left.y)) == type
		&& my_map.getCase(static_cast<int>(point_down_left.x - (MOVE_SPEED)), static_cast<int>(point_down_left.y)) == type)
		return true;
	if (move_right && my_map.getCase(static_cast<int>(point_top_right.x + (MOVE_SPEED)), static_cast<int>(point_top_right.y)) == type
		&& my_map.getCase(static_cast<int>(point_down_right.x) + (MOVE_SPEED), static_cast<int>(point_top_right.y)) == type)
		return true;
	if (move_up && my_map.getCase(static_cast<int>(point_top_left.x), static_cast<int>(point_top_left.y - (MOVE_SPEED))) == type
		&& my_map.getCase(static_cast<int>(point_top_right.x), static_cast<int>(point_top_right.y - (MOVE_SPEED))) == type)
		return true;
	if (move_down && my_map.getCase(static_cast<int>(point_down_left.x), static_cast<int>(point_down_left.y + (MOVE_SPEED))) == type
		&& my_map.getCase(static_cast<int>(point_down_right.x), static_cast<int>(point_down_right.y + (MOVE_SPEED))) == type)
		return true;
	return false;
}


void level::collision()
{
	sf::Vector2f	point_top_left;
	sf::Vector2f	point_top_right;
	sf::Vector2f	point_down_left;
	sf::Vector2f	point_down_right;

	point_top_left.x = my_player.getPosition().x + 0.1;
	point_top_left.y = my_player.getPosition().y + 0.1;
	point_top_right.x = my_player.getPosition().x + 0.9;
	point_top_right.y = my_player.getPosition().y + 0.1;
	point_down_left.x = my_player.getPosition().x + 0.1;
	point_down_left.y = my_player.getPosition().y + 0.9;
	point_down_right.x = my_player.getPosition().x + 0.9;
	point_down_right.y = my_player.getPosition().y + 0.9;

	if (move_left && my_map.getCase(point_top_left.x - (MOVE_SPEED), point_top_left.y) != Block
		&& my_map.getCase(point_down_left.x - (MOVE_SPEED), point_down_left.y) != Block)
		my_player.move_left();
	if (move_right && my_map.getCase(point_top_right.x + (MOVE_SPEED), point_top_right.y) != Block
		&& my_map.getCase(point_down_right.x + (MOVE_SPEED), point_top_right.y) != Block)
		my_player.move_right();
	if (move_up && my_map.getCase(point_top_left.x, point_top_left.y - (MOVE_SPEED)) != Block
		&& my_map.getCase(point_top_right.x, point_top_right.y - (MOVE_SPEED)) != Block)
		my_player.move_up();
	if (move_down && my_map.getCase(point_down_left.x, point_down_left.y + (MOVE_SPEED)) != Block
		&& my_map.getCase(point_down_right.x, point_down_right.y + (MOVE_SPEED)) != Block)
		my_player.move_down();
}

void level::draw_map()
{
	int	x = 0;
	int y = 0;
	while (y < 40)
	{
		x = 0;
		while (x < 40)
		{
			switch (my_map.getCase(x, y)) {
			case Free:
				entity_["free"]->set_position((x * ENTITY_SIZE), (y * ENTITY_SIZE));
				window_.draw(entity_["free"]->get());
				break;
			case Block:
				entity_["block"]->set_position((x * ENTITY_SIZE), (y * ENTITY_SIZE));
				window_.draw(entity_["block"]->get());
				break;
			case Portoloin:
				entity_["portoloin"]->set_position((x * ENTITY_SIZE), (y * ENTITY_SIZE));
				window_.draw(entity_["portoloin"]->get());
				break;
			case Finish:
				entity_["finish"]->set_position((x * ENTITY_SIZE), (y * ENTITY_SIZE));
				window_.draw(entity_["finish"]->get());
				break;			
			default:
				std::cout << "error" << std::endl;
			}
			x++;
		}
		y++;
	}
}

void level::draw_player()
{
	entity_["player"]->set_position(my_player.getPosition().x * ENTITY_SIZE, my_player.getPosition().y * ENTITY_SIZE);
	window_.draw(entity_["player"]->get());
}

void level::draw()
{
	window_.clear(sf::Color::White);
	draw_map();
	draw_player();
	window_.display();
}
