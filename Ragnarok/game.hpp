#pragma once
# include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
# include <map>
# include "Agame_state.hpp"


class game
{
public:
	game(int const width, int const heigh, std::string const &name,int const vm = sf::Style::Default);
	~game();
	bool		get_vsync() const;
	void		set_vsync(bool const status);
	int			get_fps_limit() const;
	void		set_fps_limit(int const value);
	Agame_state	*get_state(std::string const& name) const;
	void		set_state(std::string name, Agame_state *elem);
	void		set_cursor(bool const value);
	void		init_game();
	void		run();
	bool		is_running() const;
	void		change_state(std::string const& name);	
	void		delete_state(std::string const& name);
	sf::Clock	clock_;
	Agame_state	*current_state_ = nullptr;
private:
	sf::RenderWindow	window_;
	bool		vsync_ = true;
	int			fps_limit_ = 120;
	bool		running_ = false;

	std::map<std::string, Agame_state *>	states_;
};
