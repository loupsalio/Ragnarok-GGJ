#ifndef MAP_HPP_
# define MAP_HPP

# include <iostream>
# include <string>
# include <fstream>
# include <Vector>

# define X 40
# define Y 40

class Map
{
	std::vector< std::vector<int> >	_map;
	int								_level;

public:
	Map();
	~Map();
	void		setLevel(int value);
	void		setMap();
	void		chargeMap(std::string _filename);
	int			getCase(int x, int y);
};

#endif /* MAP_HPP_ */
