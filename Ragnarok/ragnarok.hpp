#pragma once
#pragma once

#include "Agame_state.hpp"
#include "entity.hpp"

class ragnarok : public Agame_state
{
public:
	explicit ragnarok(sf::RenderWindow &windows, game *my_game);
	virtual ~ragnarok() = default;
	void init() override;
	void traitement() override;
	void events() override;
	void draw() override;
};