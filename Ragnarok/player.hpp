#pragma once

#include "entity.hpp"

# define MOVE_SPEED		0.2


class player : public entity
{
	int				life_ = 3;
	sf::Vector2f	position_;
public:
	player(std::string const &name_entity);
	virtual ~player() = default;
	void	move_left();
	void	move_right();
	void	move_up();
	void	move_down();
	sf::Vector2f const &getPosition() const;
	void	lost_life();
	int		get_life() const;
	void	set_player_position(int const x, int const y);
};
